import {createTheme} from "@mui/material/styles";
import theme from "./theme";
import {faIR} from "@mui/x-date-pickers/locales";

const themeRtl = createTheme({
    direction: "rtl",
    typography: {
        fontFamily: `IRANSansFaNum, sans-serif`,
    },
    faIR,
    ...theme,
});

export default themeRtl;

import SuccessNotification from "@/core/components/notifications/SuccessNotification";
import {toast} from "react-toastify";

export const successRequest = (response, t, options) => {
    if (options.notification && options.success.notification.show) {
        toast.dismiss();
        SuccessNotification(t, response.status)
    }
}
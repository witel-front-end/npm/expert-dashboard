import ErrorNotification from "@/core/components/notifications/ErrorNotification";
import WarningNotification from "@/core/components/notifications/WarningNotification";
import {toast} from "react-toastify";

export const errorSetting = (t, notification) => {
    if (notification) toast.dismiss();
}

export const errorRequest = (t, notification) => {
    if (notification) toast.dismiss();
}

export const errorResponse = (response, clearToken, t, notification) => {
    if (notification) toast.dismiss();
    if (isServerError(response.status)) {
        errorServer(response, t, notification)
    } else if (isClientError(response.status)) {
        errorClient(response, clearToken, t, notification)
    }
}

const errorServer = (response, t, notification) => {
    if (notification) WarningNotification(t, response.status);
}
const errorClient = (response, clearToken, t, notification) => {
    switch (response.status) {
        case 401:
            clearToken()
            if (notification) ErrorNotification(t, response.status)
            break;
        case 422:
            if ('type' in response.data) {
                errorLogic(response, t, notification)
                break;
            }
            errorValidation(response, t, notification)
            break;
        case 429:
            if (notification) ErrorNotification(t, response.status)
            break
        default:
            if (notification) ErrorNotification(t, response.status)
            break
    }
}

const isServerError = status => status >= 500 && status <= 599;
const isClientError = status => status >= 400 && status <= 499;

const errorLogic = (response, t, notification) => {
    if (notification) ErrorNotification(t, response.status)
}
const errorValidation = (response, t, notification) => {
    if (notification) {
        const errorsMap = Object.keys(response.data.errors)
        const errorsArray = response.data.errors

        errorsMap.map((item, index) => {
            ErrorNotification(t, response.status, errorsArray[item][0]);
        })
    }
}
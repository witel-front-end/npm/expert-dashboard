import SpaceDashboardIcon from "@mui/icons-material/SpaceDashboard";

const sidebarMenu = [
    [
        {
            key: "sidebar.dashboard",
            type: "page",
            route: "/dashboard",
            icon: <SpaceDashboardIcon/>,
            selected: false,
            permission: "all",
        },
    ],
];

export default sidebarMenu;

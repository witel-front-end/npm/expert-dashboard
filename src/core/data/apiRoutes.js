const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

//login
export const GET_USER_TOKEN = BASE_URL + "/dashboard/login";
//end login

//change password
export const SET_USER_PASSWORD = BASE_URL + "/dashboard/profile/change_password";
//end change password

//user data
export const GET_USER_ROUTE = BASE_URL + "/dashboard/profile/info";
//user data

//passenger office
export const GET_PASSENGER_OFFICE =
    BASE_URL + "/dashboard/passenger_office_chief/show";

export const CONFIRM_PASSENGER_OFFICE =
    BASE_URL + "/dashboard/passenger_office_chief/confirm";

export const REJECT_PASSENGER_OFFICE =
    BASE_URL + "/dashboard/passenger_office_chief/reject";
//passenger office

//passenger boss
export const GET_PASSENGER_BOSS =
    BASE_URL + "/dashboard/province_working_group/show";

export const CONFIRM_PASSENGER_BOSS =
    BASE_URL + "/dashboard/province_working_group/confirm";

export const REJECT_PASSENGER_BOSS =
    BASE_URL + "/dashboard/province_working_group/reject";
//passenger boss

//transportation assistance
export const GET_TRANSPORTATION_ASSISTANCE =
    BASE_URL + "/dashboard/transportation_assistant/show";

export const CONFIRM_TRANSPORTATION_ASSISTANCE =
    BASE_URL + "/dashboard/transportation_assistant/confirm";

export const REJECT_TRANSPORTATION_ASSISTANCE =
    BASE_URL + "/dashboard/transportation_assistant/reject";
//transportation assistance

//machinary office
export const GET_MACHINARY_OFFICE =
    BASE_URL + "/dashboard/machinery_expert/show";

export const CONFIRM_MACHINARY_OFFICE =
    BASE_URL + "/dashboard/machinery_expert/confirm";

export const REJECT_MACHINARY_OFFICE =
    BASE_URL + "/dashboard/machinery_expert/reject";
//machinary office

//commercial chief
export const GET_COMMERCIAL_CHIEF =
    BASE_URL + "/dashboard/commercial_chief/show";

export const CONFIRM_COMMERCIAL_CHIEF =
    BASE_URL + "/dashboard/commercial_chief/confirm";

export const REJECT_COMMERCIAL_CHIEF =
    BASE_URL + "/dashboard/commercial_chief/reject";
//commercial chief

//navgan province manager
export const GET_NAVGAN_PROVINCE_MANAGER =
    BASE_URL + "/dashboard/province_manager/show";

export const CONFIRM_NAVGAN_PROVINCE_MANAGER =
    BASE_URL + "/dashboard/province_manager/confirm";

export const REJECT_NAVGAN_PROVINCE_MANAGER =
    BASE_URL + "/dashboard/province_manager/reject";
//navgan province manager

// refahi province manager
export const GET_REFAHI_PROVINCE_MANAGER =
    BASE_URL + "/dashboard/refahi_province_manager/show";

export const CONFIRM_REFAHI_PROVINCE_MANAGER =
    BASE_URL + "/dashboard/refahi_province_manager/confirm";

export const REJECT_REFAHI_PROVINCE_MANAGER =
    BASE_URL + "/dashboard/refahi_province_manager/reject";
//refahi province manager

// development  assistant
export const GET_DEVELOPMENT_ASSISTANT =
    BASE_URL + "/dashboard/development_assistant/show";

export const CONFIRM_DEVELOPMENT_ASSISTANT =
    BASE_URL + "/dashboard/development_assistant/confirm";

export const REJECT_DEVELOPMENT_ASSISTANT =
    BASE_URL + "/dashboard/development_assistant/reject";
//development assistant

// inspector expert
export const GET_INSPECTOR_EXPERT =
    BASE_URL + "/dashboard/inspector_expert/show";

export const CONFIRM_INSPECTOR_EXPERT =
    BASE_URL + "/dashboard/inspector_expert/confirm";

export const REJECT_INSPECTOR_EXPERT =
    BASE_URL + "/dashboard/inspector_expert/reject";

export const REVISE_INSPECTOR_EXPERT =
    BASE_URL + "/dashboard/inspector_expert/revise"
//inspector expert

// province head expert
export const GET_PROVINCE_HEAD_EXPERT =
    BASE_URL + "/dashboard/province_head_expert/show";

export const CONFIRM_PROVINCE_HEAD_EXPERT =
    BASE_URL + "/dashboard/province_head_expert/confirm";

export const REJECT_PROVINCE_HEAD_EXPERT =
    BASE_URL + "/dashboard/province_head_expert/reject";

export const REVISE_PROVINCE_HEAD_EXPERT =
    BASE_URL + "/dashboard/province_head_expert/revise";
//province head expert

//sidebar notification
export const GET_SIDEBAR_NOTIFICATION =
    BASE_URL + "/dashboard/notification"
//sidebar notification

//loan management refahi
export const GET_LOAN_MANAGEMENT_REFAHI =
    BASE_URL + "/dashboard/refahi_loans"
export const UPDATE_LOAN_MANAGEMENT_REFAHI =
    BASE_URL + "/dashboard/refahi_loans/update"
export const GET_LOAN_STATE_REFAHI =
    BASE_URL + "/dashboard/loan_states/refahi"
//loan management refahi

// loan management navgan
export const GET_LOAN_MANAGEMENT_NAVGAN =
    BASE_URL + "/dashboard/navgan_loans"
export const UPDATE_LOAN_MANAGEMENT_NAVGAN =
    BASE_URL + "/dashboard/navgan_loans/update"
export const GET_LOAN_STATE_NAVGAN =
    BASE_URL + "/dashboard/loan_states/navgan"
//loan management navgan

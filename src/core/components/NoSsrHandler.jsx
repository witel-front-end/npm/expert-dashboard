import {NoSsr} from "@mui/material";

const NoSsrHandler = ({isBot, children}) => {
    if (isBot) return children;
    return <NoSsr>{children}</NoSsr>;
};

export default NoSsrHandler;

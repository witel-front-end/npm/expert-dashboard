import {Box, Button, Paper, Typography} from "@mui/material";
import {useTranslations} from "next-intl";
import AddIcon from "@mui/icons-material/Add";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import {useRef} from "react";

const UploadSystem = ({
                          selectedImage,
                          setselectedImage,
                          handleUploadChange,
                          fieldname,
                          setFieldValue,
                          imageAlt,
                          imageSize,
                          fileType,
                          fileName,
                          setShowAddIcon,
                          showAddIcon,
                      }) => {
    const t = useTranslations();
    const fileInputRef = useRef(null);

    const handleClick = () => {
        fileInputRef.current.click();
    };

    const handleDeleteImage = () => {
        setselectedImage(null);
        setFieldValue(fieldname, null);
        setShowAddIcon(true);
        if (fileInputRef.current) {
            fileInputRef.current.value = "";
        }
    };

    const isDocumentFormat = (fileType) => {
        const documentFormats = [
            "application/pdf",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        ];
        return documentFormats.includes(fileType);
    };

    return (
        <Box sx={{width: "100%", my: 1}}>
            {showAddIcon ? (
                // Show the add icon and "Upload File" text when no image is selected
                <>
                    <Box
                        sx={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            justifyContent: "center",
                            border: "1px solid #b3b3b3",
                            borderRadius: "10px",
                            cursor: "pointer",
                            padding: "5px",

                            height: imageSize[1],
                        }}
                        onClick={handleClick}
                    >
                        <AddIcon sx={{fontSize: "2rem", color: "#a19d9d"}}/>
                        <Typography
                            variant="subtitle2"
                            sx={{
                                fontWeight: 600,
                                fontSize: "1rem",
                                color: "#a19d9d",
                                mt: 1,
                            }}
                            textAlign="center"
                        >
                            {t("UploadSystem.upload_file")}
                        </Typography>
                    </Box>
                </>
            ) : (
                // Show the uploaded content along with the delete button when an image or document is selected
                <>
                    {fileType && fileType.startsWith("image/") ? (
                        <Box
                            width="100%"
                            height={imageSize[1]}
                            sx={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                justifyContent: "center",
                                cursor: "pointer",
                                objectFit: "contain",
                                border: "1px solid #b3b3b3",
                                borderTopRightRadius: "10px",
                                borderTopLeftRadius: "10px",
                                borderBottom: "unset",
                                padding: "5px",
                                overflow: "hidden",
                            }}
                            onClick={handleClick}
                        >
                            <Paper
                                elevation={0}
                                sx={{
                                    width: "100%",
                                    height: "100%",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    backgroundSize: "contain",
                                    backgroundRepeat: "no-repeat",
                                    backgroundPosition: "center",
                                    backgroundImage: `url(${selectedImage})`,
                                }}
                            ></Paper>
                        </Box>
                    ) : (
                        fileType &&
                        isDocumentFormat(fileType) && (
                            <Box
                                sx={{
                                    height: imageSize[1],
                                    display: "flex",
                                    border: "1px solid #b3b3b3",
                                    borderTopRightRadius: "10px",
                                    borderTopLeftRadius: "10px",
                                    borderBottom: "unset",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    cursor: "pointer",
                                }}
                                onClick={handleClick}
                            >
                                <Typography
                                    margin={2}
                                    sx={{
                                        fontWeight: 600,
                                        fontSize: "1rem",
                                        color: "#a19d9d",
                                    }}
                                    textAlign="center"
                                >
                                    {fileName}
                                </Typography>
                            </Box>
                        )
                    )}
                    <Button
                        sx={{
                            width: "100%",
                        }}
                        color="error"
                        endIcon={<DeleteForeverIcon/>}
                        variant="contained"
                        onClick={handleDeleteImage}
                    >
                        {t("UploadSystem.delete")}
                    </Button>
                </>
            )}
            <input
                type="file"
                accept="image/*, .pdf, .doc, .docx, .xls, .xlsx"
                style={{display: "none"}}
                onChange={handleUploadChange}
                ref={fileInputRef}
            />
        </Box>
    );
};

export default UploadSystem;

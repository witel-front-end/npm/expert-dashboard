import {Avatar, Box, TextField} from "@mui/material";
import {useState} from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";

const AvatarUpload = ({user, setFieldValue, valueAvatar, changeFlag}) => {
    const [selectedImage, setSelectedImage] = useState(user.expert_avatar);
    const [isHovered, setIsHovered] = useState(false);

    const handleImageChange = (event) => {
        const newImage = event.target?.files?.[0];
        if (newImage) {
            setSelectedImage(URL.createObjectURL(newImage));
            setFieldValue(valueAvatar, newImage);
            setFieldValue(changeFlag, true);
        } else {
            setSelectedImage("");
            setFieldValue(valueAvatar, null);
        }
    };

    const handleDeleteImage = () => {
        setSelectedImage("");
        setFieldValue(valueAvatar, null);
        setFieldValue(changeFlag, true);
    };

    const handleMouseEnter = () => {
        setIsHovered(true);
    };

    const handleMouseLeave = () => {
        setIsHovered(false);
    };

    return (
        <Box
            sx={{
                display: "inline-block",
                position: "relative",
            }}
        >
            <Box
                component="label"
                htmlFor="avatar-upload"
                sx={{
                    display: "inline-block",
                    width: "fit-content",
                    height: "fit-content",
                    cursor: "pointer",
                }}
            >
                <Box
                    component="div"
                    className={`avatar-container ${isHovered ? "hovered" : ""}`}
                    onMouseEnter={handleMouseEnter}
                    onMouseLeave={handleMouseLeave}
                    sx={{
                        position: "relative",
                        width: 150,
                        height: 150,
                        borderRadius: "50%",
                        overflow: "hidden",
                        transition: "transform 0.3s ease-in-out",
                    }}
                >
                    <Avatar
                        alt="User Avatar"
                        src={selectedImage}
                        sx={{
                            width: 150,
                            height: 150,
                            cursor: "pointer",
                            position: "relative",
                        }}
                    />
                    {isHovered && (
                        <Box
                            component="div"
                            className="avatar-overlay"
                            sx={{
                                position: "absolute",
                                top: 0,
                                left: 0,
                                width: "100%",
                                height: "100%",
                                backgroundColor: "rgba(0, 0, 0, 0.6)",
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                transition: "transform 0.3s ease-in-out",
                                transform: `scale(${isHovered ? 1 : 0})`,
                            }}
                        >
                            {selectedImage ? (
                                <DeleteIcon
                                    sx={{
                                        color: "#fff",
                                        width: 35,
                                        height: 35,
                                    }}
                                    onClick={handleDeleteImage}
                                />
                            ) : (
                                <AddIcon
                                    sx={{
                                        color: "#fff",
                                        width: 35,
                                        height: 35,
                                    }}
                                />
                            )}
                        </Box>
                    )}
                </Box>
            </Box>
            <TextField
                id="avatar-upload"
                type="file"
                accept="image/*"
                sx={{display: "none"}}
                onChange={handleImageChange}
            />
        </Box>
    );
};

export default AvatarUpload;

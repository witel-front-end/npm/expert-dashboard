import ImageResize from "image-resize";

const ImageResizer = async (image) => {
    const imageResize = new ImageResize({
        quality: 1,
        format: "jpg",
        outputType: "base64",
        width: 400,
    });

    const get = await imageResize.get(image);
    const resize = await imageResize.resize(get).then();
    const output = await imageResize.output(resize).then();

    const avatar = await (await import("image-to-file-converter"))
        .base64ToFile(output)
        .then();

    return avatar;
};

export default ImageResizer;

import {InputAdornment, TextField, Typography} from "@mui/material";
import {useTranslations} from "next-intl";

const PriceField = (props) => {
    const t = useTranslations();
    return (
        <TextField
            InputProps={{
                endAdornment: (
                    <InputAdornment position="end">
                        <Typography
                            sx={{margin: 1}}
                            component="span">{((props.value) / 10).toLocaleString()}</Typography>
                        <Typography component="span"
                                    variant="caption">{t("ConfirmDialog.toman")}</Typography>
                    </InputAdornment>
                ),
            }}
            {...props}
        />
    )
}
export default PriceField
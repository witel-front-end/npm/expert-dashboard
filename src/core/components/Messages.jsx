import CenterLayout from "@/layouts/CenterLayout";
import FullPageLayout from "@/layouts/FullPageLayout";
import SvgLoading from "@/core/components/svgs/SvgLoading";

const Message = ({text, actions}) => {
    return (
        <FullPageLayout sx={{p: 1}}>
            <CenterLayout spacing={3}>
                <SvgLoading width={100}
                            height={100}/>
                {text}
                {actions}
            </CenterLayout>
        </FullPageLayout>
    );
};

export default Message;

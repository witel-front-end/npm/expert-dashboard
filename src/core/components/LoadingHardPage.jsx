import {Backdrop, Box, styled} from "@mui/material";
import SvgLoading from "@/core/components/svgs/SvgLoading";

const LoadingImage = styled(Box)({
    "@keyframes load": {
        "0%": {
            // opacity: 0,
            transform: "scale(1)",
        },
        "50%": {
            // opacity: 1,
            transform: "scale(2)",
        },
        "100%": {
            // opacity: 0,
            transform: "scale(1)",
        },
    },
    animation: "load 2s infinite",
});

const LoadingHardPage = ({children, loading}) => {
    return (
        <>
            <Backdrop
                sx={{bgcolor: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1}}
                open={loading}
            >
                <LoadingImage
                    width={100}
                    height={100}
                >
                    <SvgLoading width={100}
                                height={100}/>
                </LoadingImage>
            </Backdrop>
            {children}
        </>
    );
};

export default LoadingHardPage;

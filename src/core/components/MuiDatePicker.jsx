import {LocalizationProvider, MobileDateTimePicker} from "@mui/x-date-pickers";
import {AdapterDateFnsJalali} from "@mui/x-date-pickers/AdapterDateFnsJalali";
import moment from "jalali-moment";
import {faIR} from "@mui/x-date-pickers/locales";
import {Box, IconButton} from "@mui/material";
import ClearIcon from "@mui/icons-material/Clear";
import {useTranslations} from "next-intl";

export default function MuiDatePicker({column}) {
    const t = useTranslations();
    const filterFnValue = column.columnDef._filterFn;

    return (
        <Box sx={{display: "flex", alignItems: "start"}}>
            <LocalizationProvider
                dateAdapter={AdapterDateFnsJalali}
                localeText={
                    faIR.components.MuiLocalizationProvider.defaultProps
                        .localeText
                }
            >
                <MobileDateTimePicker
                    ampm={false}
                    onChange={(newValue) => {
                        const date = new Date(newValue);
                        const formattedDate = moment(date)
                            .locale("en")
                            .format("YYYY-MM-DD HH:mm");
                        column.setFilterValue(formattedDate);
                    }}
                    slotProps={{
                        textField: {
                            placeholder: "تاریخ خود را وارد کنید",
                            helperText: `${t("filter_mode")}: ${t(filterFnValue)}`,
                            sx: {minWidth: "120px"},
                            variant: "standard",
                        },
                    }}
                    value={
                        column.getFilterValue()
                            ? new Date(column.getFilterValue())
                            : null
                    }
                />
            </LocalizationProvider>
            <IconButton
                size="small"
                onClick={() => {
                    column.setFilterValue(null);
                }}
                sx={{
                    color: column.getFilterValue()
                        ? "rgba(0, 0, 0, 0.54)"
                        : "#bfbfbf",
                }}
            >
                <ClearIcon/>
            </IconButton>
        </Box>
    );
}

import {toast} from "react-toastify";

const PendingNotification = (t) => {
    toast(t("notifications.pending"), {
        autoClose: false,
        closeButton: false,
        closeOnClick: false,
        draggable: false,
    });
};

export default PendingNotification;

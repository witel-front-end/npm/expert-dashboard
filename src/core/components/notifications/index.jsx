import {toast} from "react-toastify";
import ErrorNotification from "./ErrorNotification";
import WarningNotification from "./WarningNotification";
import SuccessNotification from "./SuccessNotification";

const Notifications = async (t, response) => {
    const {status, data} = response != undefined ? response : ""
    toast.dismiss();
    switch (status) {
        case 200:
            SuccessNotification(t, status);
            break;
        case 400:
            ErrorNotification(t, status);
            break;
        case 401:
            ErrorNotification(t, status);
            break;
        case 403:
            ErrorNotification(t, status);
            break;
        case 422:
            ErrorNotification(t, status, data.message);
            break;
        case 500:
            WarningNotification(t, status);
            break;
        case 503:
            WarningNotification(t, status);
            break;
        case 504:
            WarningNotification(t, status);
            break;
        default:
            toast(t("notifications.pending"), {
                autoClose: false,
                closeOnClick: false,
                draggable: false,
            });
            break;
    }
};

export default Notifications;

/*
usage document

** for pending use ( Notifications( t, undefined) ) this before your request.
** for success use ( Notifications( t, response) ) this inside .then() of your request.
** for Error and Warning use ( Notifications( t, error.response) ) this inside .catche() of your request.

end usage document
*/

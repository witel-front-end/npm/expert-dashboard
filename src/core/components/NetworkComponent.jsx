import useNetwork from "@/lib/app/hooks/useNetwork";
import {useEffect, useRef} from "react";
import {toast} from "react-toastify";
import WifiIcon from '@mui/icons-material/Wifi';
import WifiOffIcon from '@mui/icons-material/WifiOff';
import {useTranslations} from "next-intl";

const NetworkComponent = () => {
    const toastId = useRef(null);
    const network = useNetwork()
    const t = useTranslations()

    useEffect(() => {
        if (network.online) {
            toast.update(toastId.current, {
                type: toast.TYPE.SUCCESS,
                render: t('online_message'),
                autoClose: 2000,
                closeButton: true,
                closeOnClick: true,
                icon: <WifiIcon/>
            });
            return
        }
        toast.dismiss()
        toastId.current = toast.warn(t('offline_message'), {
            autoClose: false, closeButton: false, closeOnClick: false, icon: <WifiOffIcon/>
        })
    }, [network.online]);

    return ''
}

export default NetworkComponent
import {styled} from "@mui/material";
import Image from "next/image";

const StyledImage = styled(Image)``;

export default StyledImage;

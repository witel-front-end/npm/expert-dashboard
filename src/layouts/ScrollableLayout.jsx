import {Box, Fade} from "@mui/material";

const ScrollableLayout = (props) => {
    const overflowY = props?.y || "hidden";
    const overflowX = props?.x || "hidden";

    return (
        <Fade in={true}>
            <Box
                spacing={props?.spacing}
                sx={{
                    width: "auto",
                    height: "auto",
                    overflowY: overflowY,
                    overflowX: overflowX,
                    ...props?.sx,
                }}
            >
                {props.children}
            </Box>
        </Fade>
    );
};

export default ScrollableLayout;

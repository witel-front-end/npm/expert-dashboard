import {Stack} from "@mui/material";

const FullPageLayout = (props) => {
    return (
        <Stack
            spacing={props?.spacing}
            direction={props?.direction}
            sx={{
                width: "100%",
                height: "100%",
                overflowY: "scroll",
                overflowX: "scroll",
                ...props?.sx,
            }}
        >
            {props.children}
        </Stack>
    );
};

export default FullPageLayout;

import NoSsrHandler from "@/core/components/NoSsrHandler";
import {createEmotionCacheRtl} from "@/core/utils/createEmotionCache";
import themeRtl from "@/core/utils/theme-rtl";
import {CacheProvider} from "@emotion/react";
import {GlobalStyles} from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import {ThemeProvider} from "@mui/material/styles";
import Head from "next/head";

const clientSideEmotionCacheRtl = createEmotionCacheRtl();

const MuiLayout = ({children, isBot}) => {
    const emotionCache = clientSideEmotionCacheRtl;
    const theme = themeRtl;

    return (
        <NoSsrHandler isBot={isBot}>
            <CacheProvider value={emotionCache}>
                <Head>
                    <meta name="viewport" content="initial-scale=1, width=device-width"/>
                </Head>
                <ThemeProvider theme={theme}>
                    <GlobalStyles
                        styles={{
                            "*:not(.MuiTableContainer-root)::-webkit-scrollbar": {
                                display: "none",
                            },
                            "*::-webkit-scrollbar": {
                                height: "8px",
                            },
                            "*::-webkit-scrollbar-thumb": {
                                background: `${theme.palette.primary.light}80`,
                                borderRadius: "3px",
                            },
                            "*:not(.MuiTableContainer-root)": {
                                scrollbarWidth: "thin",
                                scrollbarColor: "transparent transparent",
                            },

                            "*": {
                                scrollbarWidth: "thin",
                                scrollbarColor: `${theme.palette.primary.light}80 transparent`,
                            },

                            "*::-moz-scrollbar-thumb": {
                                backgroundColor: `${theme.palette.primary.light}80`,
                            },
                            [`@media (max-width: ${theme.breakpoints.values.sm}px)`]: {
                                "*::-webkit-scrollbar": {
                                    height: "4px",
                                },
                            },

                            body: {
                                width: "100vw",
                                height: "100vh",
                            },
                            "#__next": {
                                width: "100%",
                                height: "100%",
                            },
                        }}
                    />

                    <CssBaseline/>
                    {children}
                </ThemeProvider>
            </CacheProvider>
        </NoSsrHandler>
    );
};

export default MuiLayout;

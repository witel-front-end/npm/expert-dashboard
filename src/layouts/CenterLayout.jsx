import {Fade, Stack} from "@mui/material";

const CenterLayout = (props) => {
    return (
        <Fade in={true}>
            <Stack
                alignItems="center"
                justifyContent="center"
                spacing={props?.spacing}
                sx={{flex: 1, ...props?.sx, py: 3}}
            >
                {props.children}
            </Stack>
        </Fade>
    );
};

export default CenterLayout;

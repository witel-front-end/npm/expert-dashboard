import {useState} from "react";
import FullPageLayout from "../FullPageLayout";
import Header from "./header";
import Sidebar from "./sidebar";
import {Toolbar} from "@mui/material";
import BreadCrumbs from "./breadcrumbs";

const drawerWidth = 240;

const DashboardLayouts = (props) => {
    const {window} = props;
    const [mobileOpen, setMobileOpen] = useState(false);
    const container =
        window !== undefined ? () => window().document.body : undefined;

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    return (
        <FullPageLayout direction="row">
            <Header
                handleDrawerToggle={handleDrawerToggle}
                drawerWidth={drawerWidth}
            />
            <Sidebar
                container={container}
                mobileOpen={mobileOpen}
                handleDrawerToggle={handleDrawerToggle}
                drawerWidth={drawerWidth}
            />
            <FullPageLayout
                component="main"
                sx={{flexGrow: 1, width: {sm: `calc(100% - ${drawerWidth}px)`}}}
            >
                <Toolbar/>
                <FullPageLayout sx={{mt: 3}}>
                    <BreadCrumbs isVisible={true}/>
                    {props.children}
                </FullPageLayout>
            </FullPageLayout>
        </FullPageLayout>
    );
};

export default DashboardLayouts;

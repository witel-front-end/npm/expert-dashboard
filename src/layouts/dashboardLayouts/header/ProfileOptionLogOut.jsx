import {Box, Button, MenuItem, Typography} from "@mui/material";
import MeetingRoomIcon from "@mui/icons-material/MeetingRoom";
import useUser from "@/lib/app/hooks/useUser";
import {useTranslations} from "next-intl";

export default function ProfileOptionLogOut({handleCloseUserMenu}) {
    const t = useTranslations();
    const {clearToken} = useUser();
    const handleClickLogOut = () => {
        handleCloseUserMenu();
        clearToken();
    };
    return (
        <>
            <MenuItem
                component={Button}
                to={{
                    pathname: "/dashboard/logout",
                }}
                sx={{
                    display: "flex",
                    justifyContent: "center",
                    borderTop: 1,
                    px: 3,
                    py: 1.5,
                    borderColor: "#e1e1e1",
                    textTransform: "unset",
                }}
                onClick={handleClickLogOut}
            >
                <Box sx={{display: "flex", alignItems: "center", flex: 1}}>
                    <Box
                        sx={{
                            display: "flex",
                            alignItems: "center",
                            color: "primary.main",
                            pr: 2,
                        }}
                    >
                        <MeetingRoomIcon/>
                    </Box>
                    <Typography sx={{flex: 1}} textAlign="start">
                        {t("header.logout")}
                    </Typography>
                </Box>
            </MenuItem>
        </>
    );
}

import {NextLinkComposed} from "@/core/components/LinkRouting";
import {
    Badge,
    Collapse,
    IconButton,
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    Typography,
} from "@mui/material";
import {useTranslations} from "next-intl";
import useNotification from "@/lib/app/hooks/useNotification";

const SidebarListSubItem = ({item, handleDrawerToggle}) => {
    const t = useTranslations();
    const {notification_count} = useNotification()

    return (
        <Collapse in={item.showSubItem} timeout="auto" mountOnEnter={true} unmountOnExit={true}>
            <List component="div" disablePadding sx={{bgcolor: "#f6f6f6", pr: 1}}>
                {item.subItem.map((subitem, index) => (
                    <ListItem key={subitem.key} disablePadding secondaryAction={
                        <IconButton>
                            <Badge
                                badgeContent={notification_count ? notification_count[subitem.name] : 0}
                                color="error"
                                variant="standard"
                                anchorOrigin={{
                                    vertical: "top",
                                    horizontal: "right",
                                }}
                            />
                        </IconButton>
                    }>
                        <ListItemButton
                            selected={subitem.selected}
                            component={NextLinkComposed}
                            to={{
                                pathname: subitem.route,
                            }}
                            sx={{
                                minHeight: 48,
                            }}
                        >
                            <ListItemIcon
                                sx={{
                                    minWidth: 0,
                                    justifyContent: "center",
                                    pr: 2,
                                }}
                            >
                                {subitem.icon}
                            </ListItemIcon>
                            <ListItemText primary={t(subitem.key)} secondary={
                                subitem.secondary !== undefined ? (
                                    <Typography variant="caption" color="textSecondary">
                                        {t(subitem.secondary)}
                                    </Typography>
                                ) : null
                            }/>
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Collapse>
    );
};

export default SidebarListSubItem;

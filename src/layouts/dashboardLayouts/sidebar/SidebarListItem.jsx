import {NextLinkComposed} from "@/core/components/LinkRouting";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import {Badge, IconButton, ListItem, ListItemButton, ListItemIcon, ListItemText, Typography,} from "@mui/material";
import {useTranslations} from "next-intl";
import {Fragment} from "react";
import SidebarListSubItem from "./SidebarListSubItem";
import useNotification from "@/lib/app/hooks/useNotification";

const SidebarListItem = ({item, dispatch, handleDrawerToggle}) => {
    const t = useTranslations();
    const {notification_count} = useNotification()

    return (
        <>
            <ListItem disablePadding secondaryAction={
                <IconButton>
                    <Badge
                        badgeContent={notification_count ? notification_count[item.name] : 0}
                        color="error"
                        variant="standard"
                        anchorOrigin={{
                            vertical: "top",
                            horizontal: "right",
                        }}
                    />
                </IconButton>
            }>
                <ListItemButton
                    selected={item.selected}
                    {...(item.type == "page" && {
                        component: NextLinkComposed,
                        to: {
                            pathname: item.route,
                        },
                    })}
                    onClick={() => {
                        if (item.type == "menu") {
                            dispatch({type: "COLLAPSE_MENU", key: item.key});
                        }
                        handleDrawerToggle();
                    }}
                    sx={{
                        minHeight: 48,
                    }}

                >
                    <ListItemIcon
                        sx={{
                            minWidth: 0,
                            justifyContent: "center",
                            color: "primary.main",
                            pr: 2,
                        }}
                    >
                        {item.icon}
                    </ListItemIcon>
                    <ListItemText
                        primary={t(item.key)}
                        secondary={
                            item.secondary !== undefined ? (
                                <Typography variant="caption" color="textSecondary">
                                    {t(item.secondary)}
                                </Typography>
                            ) : null
                        }
                    />


                    {item.type == "menu" &&
                        (item.showSubItem ? <ExpandLess/> : <ExpandMore/>)}
                </ListItemButton>
            </ListItem>
            {item.subItem && (
                <SidebarListSubItem
                    item={item}
                    handleDrawerToggle={handleDrawerToggle}
                />
            )}
        </>
    );
};

export default SidebarListItem;

import {Divider, Stack, Toolbar, Typography} from "@mui/material";
import {useTranslations} from "next-intl";
import SidebarList from "./SidebarList";
import useUser from "@/lib/app/hooks/useUser";

const SidebarDrawer = ({handleDrawerToggle}) => {
    const {user} = useUser()
    const t = useTranslations();
    return (
        <>
            <Toolbar>
                <Stack>
                    <Typography variant="h6" sx={{color: "primary.main"}}>
                        {t("app_short_name")}
                    </Typography>
                    <Typography variant="caption">
                        {user.name} | {user.position}
                    </Typography>
                </Stack>
            </Toolbar>
            <Divider/>
            <SidebarList handleDrawerToggle={handleDrawerToggle}/>
        </>
    );
};

export default SidebarDrawer;

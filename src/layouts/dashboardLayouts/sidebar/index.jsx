import {Box, Drawer} from "@mui/material";
import SidebarDrawer from "./SidebarDrawer";

const Sidebar = (props) => {
    return (
        <Box
            component="nav"
            sx={{width: {md: props.drawerWidth}, flexShrink: {sm: 0}}}
            aria-label="mailbox folders"
        >
            <Drawer
                container={props.container}
                variant="temporary"
                open={props.mobileOpen}
                onClose={props.handleDrawerToggle}
                ModalProps={{
                    keepMounted: true,
                }}
                sx={{
                    display: {xs: "block", md: "none"},
                    "& .MuiDrawer-paper": {
                        boxSizing: "border-box",
                        width: props.drawerWidth,
                    },
                }}
            >
                <SidebarDrawer handleDrawerToggle={props.handleDrawerToggle}/>
            </Drawer>
            <Drawer
                variant="permanent"
                sx={{
                    display: {xs: "none", md: "block"},
                    "& .MuiDrawer-paper": {
                        boxSizing: "border-box",
                        width: props.drawerWidth,
                    },
                }}
                open
            >
                <SidebarDrawer handleDrawerToggle={props.handleDrawerToggle}/>
            </Drawer>
        </Box>
    );
};

export default Sidebar;

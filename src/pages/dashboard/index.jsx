import DashboardFirstComponent from "@/components/dashboard/first";
import WithAuthMiddleware from "@/middlewares/WithAuth";
import {parse} from "next-useragent";

export default function Dashboard() {
    return (
        <WithAuthMiddleware>
            <DashboardFirstComponent/>
        </WithAuthMiddleware>
    );
}

export async function getServerSideProps({req, locale}) {
    const {isBot} = parse(req.headers["user-agent"]);
    return {
        props: {
            messages: (await import(`&/locales/${locale}/app.json`)).default,
            title: "Dashboard.dashboard_page",
            isBot,
        },
    };
}

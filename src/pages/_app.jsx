import "&/fontiran.scss";
import AppLayout from "@/layouts/AppLayout";
import MuiLayout from "@/layouts/MuiLayout";
import {LanguageProvider} from "@/lib/app/contexts/language";
import {LoadingProvider} from "@/lib/app/contexts/loading";
import {UserProvider} from "@/lib/app/contexts/user";
import "moment/locale/fa";
import {NextIntlProvider} from "next-intl";
import TitlePage from "@/core/components/TitlePage";

const App = ({Component, pageProps}) => {

    return (
        <>
            <UserProvider>
                <LanguageProvider>
                    <NextIntlProvider messages={pageProps.messages || {}}>
                        <MuiLayout isBot={pageProps.isBot}>
                            {pageProps.messages ? <TitlePage text={pageProps.title}/> : ''}
                            <LoadingProvider>
                                <AppLayout isBot={pageProps.isBot}>
                                    <Component {...pageProps} />
                                </AppLayout>
                            </LoadingProvider>
                        </MuiLayout>
                    </NextIntlProvider>
                </LanguageProvider>
            </UserProvider>
        </>
    );
};

export default App;

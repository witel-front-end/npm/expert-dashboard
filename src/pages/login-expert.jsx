import LoginComponent from "@/components/login-expert";
import WithoutAuthMiddleware from "@/middlewares/WithoutAuth";
import {parse} from "next-useragent";

export default function Login() {
    return (
        <WithoutAuthMiddleware>
            <LoginComponent/>
        </WithoutAuthMiddleware>
    );
}

export async function getServerSideProps({req, locale}) {
    const {isBot} = parse(req.headers["user-agent"]);
    return {
        props: {
            messages: (await import(`&/locales/${locale}/app.json`)).default,
            title: "Titles.title_login_expert_page",
            isBot,
        },
    };
}

import FirstComponent from "@/components/first";
import {parse} from "next-useragent";

export default function Home() {
    return (
        <>
            <FirstComponent/>
        </>
    );
}

export async function getServerSideProps({req, locale}) {
    const {isBot} = parse(req.headers["user-agent"]);
    return {
        props: {
            messages: (await import(`&/locales/${locale}/app.json`)).default,
            title: "first_page",
            isBot,
        },
    };
}

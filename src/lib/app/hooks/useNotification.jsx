import useSWR from 'swr'
import {GET_SIDEBAR_NOTIFICATION} from "@/core/data/apiRoutes";
import useRequest from "@/lib/app/hooks/useRequest";

const useNotification = () => {
    const requestServer = useRequest({auth: true, notification: false})

    //swr config
    const fetcher = (...args) => {
        return requestServer(args, 'get').then((response) => {
            return response.data.data;
        }).catch(() => {
        })
    };

    const {data, mutate} = useSWR(GET_SIDEBAR_NOTIFICATION, fetcher)
    const notification_count = data
    //swr config

    // render data
    return {notification_count, update_notification: mutate}
}
export default useNotification
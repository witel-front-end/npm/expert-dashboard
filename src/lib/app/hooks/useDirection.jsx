import {useContext} from "react";
import {LanguageContext} from "../contexts/language";

const useDirection = () => {
    const {directionApp} = useContext(LanguageContext);

    return {directionApp};
};

export default useDirection;

import useSWR from 'swr'
import {GET_LOAN_STATE_NAVGAN} from "@/core/data/apiRoutes";
import useRequest from "@/lib/app/hooks/useRequest";

const useLoanStateNavgan = () => {
    const requestServer = useRequest({auth: true, notification: false})

    //swr config
    const fetcher = (...args) => {
        return requestServer(args, 'get').then((response) => {
            return response.data.data;
        }).catch(() => {
        })
    };

    const {data} = useSWR(GET_LOAN_STATE_NAVGAN, fetcher, {
        revalidateIfStale: false,
        revalidateOnFocus: false,
        revalidateOnReconnect: false
    })
    const loan_state_navgan = data
    //swr config

    // render data
    return {loan_state_navgan}
}
export default useLoanStateNavgan
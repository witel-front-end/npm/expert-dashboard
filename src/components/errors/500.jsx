import CenterLayout from "@/layouts/CenterLayout";
import FullPageLayout from "@/layouts/FullPageLayout";
import {Box, Button, Container, Stack, Typography} from "@mui/material";
import {NextLinkComposed} from "@/core/components/LinkRouting";
import {useTranslations} from "next-intl";
import Image from "next/image";
import TitlePage from "@/core/components/TitlePage";

const ServerErrorComponent = () => {
    const t = useTranslations();

    return (
        <>
            <TitlePage text="Titles.title_custom_500"/>
            <FullPageLayout sx={{p: 1}}>
                <CenterLayout>
                    <Container maxWidth="sm">
                        <Stack spacing={4} sx={{p: 4}}>
                            <Box sx={{position: "relative", width: "100%", height: 200}}>
                                <Image
                                    fill
                                    src="/images/500.svg"
                                    alt={t("app_name")}
                                    priority
                                />
                            </Box>
                            <Typography margin={2} variant="h6" textAlign="center">
                                {t("ErrorPage.custom_500")}
                            </Typography>
                            <Button
                                variant="contained"
                                component={NextLinkComposed}
                                to={{
                                    pathname: "/",
                                }}
                            >
                                {t("ErrorPage.link_routing_back_to")}{" "}
                                {t("ErrorPage.link_routing_main_page")}
                            </Button>
                        </Stack>
                    </Container>
                </CenterLayout>
            </FullPageLayout>
        </>
    );
};

export default ServerErrorComponent;

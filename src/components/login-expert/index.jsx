import LinkRouting from "@/core/components/LinkRouting";
import PasswordField from "@/core/components/PasswordField";
import StyledForm from "@/core/components/StyledForm";
import {GET_USER_TOKEN} from "@/core/data/apiRoutes";
import CenterLayout from "@/layouts/CenterLayout";
import FullPageLayout from "@/layouts/FullPageLayout";
import useUser from "@/lib/app/hooks/useUser";
import LoginIcon from "@mui/icons-material/Login";
import {Box, Button, Container, Paper, Stack, TextField, Typography,} from "@mui/material";
import {Field, Formik} from "formik";
import {useTranslations} from "next-intl";
import {useSearchParams} from "next/navigation";
import * as Yup from "yup";
import useDirection from "@/lib/app/hooks/useDirection";
import SvgLogin from "@/core/components/svgs/SvgLogin";
import useRequest from "@/lib/app/hooks/useRequest";

const LoginComponent = () => {
    const t = useTranslations();
    const {directionApp} = useDirection();  // should delete because we don't have direction anymore
    const {setToken} = useUser(); // pass token to set token
    const requestServer = useRequest()

    // getting url query
    const searchParams = useSearchParams();
    const backUrlDecodedPath = searchParams.get("back_url");

    //formik properties
    const handleSubmit = (values, props) => {
        requestServer(GET_USER_TOKEN, 'post', {
            data: {
                username: values.username,
                password: values.password,
            },
            success: {
                notification: {show: false}
            }
        }).then((response) => {
            setToken(response.data.token)
        }).catch(() => {
            props.setSubmitting(false)
        })
    };
    const initialValues = {
        username: "",
        password: "",
    };
    const validationSchema = Yup.object().shape({
        username: Yup.string().required(t("LoginPage.error_message_required")),
        password: Yup.string().required(t("LoginPage.error_message_required")),
    });

    return (
        <FullPageLayout sx={{p: 1}}>
            <CenterLayout>
                <Container maxWidth="sm">
                    <Paper elevation={0}>
                        <Formik
                            initialValues={initialValues}
                            onSubmit={handleSubmit}
                            validationSchema={validationSchema}
                        >
                            {(props) => (
                                <Stack spacing={2} sx={{p: 2}}>
                                    <Stack
                                        sx={{width: "100%"}}
                                        alignItems='center'
                                    >
                                        <SvgLogin width={300} height={200}/>
                                    </Stack>
                                    <Typography margin={2} variant="h4" textAlign="center">
                                        {t("login_expert")}
                                    </Typography>
                                    <StyledForm sx={{width: "100%"}}>
                                        <Stack spacing={3} sx={{p: 2}}>
                                            <Field
                                                as={TextField}
                                                name="username"
                                                variant="outlined"
                                                label={t("LoginPage.text_field_user_name")}
                                                placeholder={t(
                                                    "LoginPage.text_field_enter_your_username"
                                                )}
                                                type={"text"}
                                                error={
                                                    props.touched.username && props.errors.username
                                                        ? true
                                                        : false
                                                }
                                                fullWidth
                                                helperText={
                                                    props.touched.username ? props.errors.username : null
                                                }
                                            />
                                            <PasswordField
                                                name="password"
                                                label={t("LoginPage.text_field_password")}
                                                error={
                                                    props.touched.password && props.errors.password
                                                        ? true
                                                        : false
                                                }
                                                helperText={
                                                    props.touched.password ? props.errors.password : null
                                                }
                                                placeholder={t(
                                                    "LoginPage.text_field_enter_your_password"
                                                )}
                                            />
                                            <Box
                                                sx={{
                                                    display: "flex",
                                                    alignItems: "center",
                                                    justifyContent: "space-between",
                                                }}
                                            >
                                                <Button
                                                    type="submit"
                                                    variant="contained"
                                                    fullWidth
                                                    size="medium"
                                                    endIcon={<LoginIcon/>}
                                                    disabled={props.isSubmitting}
                                                >
                                                    {t("LoginPage.button_submit")}
                                                </Button>
                                            </Box>
                                        </Stack>
                                    </StyledForm>
                                </Stack>
                            )}
                        </Formik>
                    </Paper>
                </Container>
            </CenterLayout>
            <Stack direction="row" alignItems="center" justifyContent="center">
                <LinkRouting
                    sx={{margin: 2}}
                    href={
                        backUrlDecodedPath ? decodeURIComponent(backUrlDecodedPath) : "/"
                    }
                >
                    {t("LoginPage.link_routing_back_to")}{" "}
                    {backUrlDecodedPath
                        ? t("LoginPage.link_routing_previuos_page")
                        : t("LoginPage.link_routing_main_page")}
                </LinkRouting>
            </Stack>
        </FullPageLayout>
    );
};

export default LoginComponent;

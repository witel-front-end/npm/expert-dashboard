import LinkRouting, { NextLinkComposed } from "@/core/components/LinkRouting";
import CenterLayout from "@/layouts/CenterLayout";
import FullPageLayout from "@/layouts/FullPageLayout";
import useUser from "@/lib/app/hooks/useUser";
import { Button, Link, Stack, Typography } from "@mui/material";
import { useTranslations } from "next-intl";
import SvgDashboard from "@/core/components/svgs/SvgDashboard";
import process from "next/dist/build/webpack/loaders/resolve-url-loader/lib/postcss";

const FirstComponent = () => {
    const t = useTranslations();
    const { isAuth } = useUser();

    return (
        <FullPageLayout sx={{ p: 1 }}>
            <CenterLayout spacing={3}>
                <SvgDashboard width={300} height={200} />
                <Typography variant="h5" sx={{ textAlign: "center" }}>
                    {t("app_name")}
                </Typography>
                {isAuth ? (
                    <Button
                        variant="outlined"
                        component={NextLinkComposed}
                        to={{
                            pathname: "/dashboard",
                        }}
                    >
                        {t("dashboard")}
                    </Button>
                ) : (
                    <Button
                        sx={{ mx: 2 }}
                        variant="contained"
                        component={NextLinkComposed}
                        to={{
                            pathname: "/login-expert",
                        }}
                    >
                        {t("login_expert")}
                    </Button>
                )}
            </CenterLayout>

            <Stack direction="row" alignItems="center" justifyContent="center">
                <Typography
                    variant={"caption"}
                    sx={{
                        color: "primary.main",
                        fontFamily: "Arial",
                        fontWeight: "bold",
                    }}
                >
                    v{process.env.NEXT_PUBLIC_API_VERSION}
                </Typography>
            </Stack>
            <Stack direction="row" alignItems="center" justifyContent="center">
                <Stack direction={"row"}>
                    <Typography
                        variant={"caption"}
                        sx={{
                            color: "primary.main",
                            fontFamily: "Arial",
                            fontWeight: "bold",
                        }}
                    >
                        Powered by <Link href="https://witel.ir" target="_blank">witel</Link>
                    </Typography>
                </Stack>
            </Stack>
        </FullPageLayout>
    );
};

export default FirstComponent;

import {NextLinkComposed} from "@/core/components/LinkRouting";
import Message from "@/core/components/Messages";
import useUser from "@/lib/app/hooks/useUser";
import {Button, Typography} from "@mui/material";
import {useTranslations} from "next-intl";
import {useRouter} from "next/router";

const WithAuthMiddleware = ({children}) => {
    const {isAuth} = useUser();
    const t = useTranslations();
    const router = useRouter();

    if (!isAuth)
        return (
            <Message
                text={
                    <Typography sx={{textAlign: "center"}}>
                        {t(
                            "Authorization.typography_your_access_to_this_page_has_expired_Please_login_again"
                        )}
                    </Typography>
                }
                actions={
                    <>
                        <Button
                            variant="contained"
                            component={NextLinkComposed}
                            to={{
                                pathname: "/login-expert",
                                query: {back_url: encodeURIComponent(router.asPath)},
                            }}
                        >
                            {t("login")}
                        </Button>
                    </>
                }
            />
        );
    return <>{children}</>;
};

export default WithAuthMiddleware;

import {NextLinkComposed} from "@/core/components/LinkRouting";
import Message from "@/core/components/Messages";
import useUser from "@/lib/app/hooks/useUser";
import {Button, Typography} from "@mui/material";
import {useTranslations} from "next-intl";

const RolePermissionMiddleware = ({children, requiredPermissions}) => {
    const {user} = useUser();
    const t = useTranslations();

    const hasPermission = requiredPermissions.some((permission) =>
        user?.permissions?.includes(permission)
    );

    if (!hasPermission) {
        return (
            <Message
                text={
                    <Typography sx={{textAlign: "center"}}>
                        {t("Permission.typography_you_dont_have_access")}
                    </Typography>
                }
                actions={
                    <>
                        <Button
                            variant="contained"
                            component={NextLinkComposed}
                            to={{
                                pathname: "/dashboard",
                            }}
                        >
                            {t("Permission.button_back_dashboard")}
                        </Button>
                    </>
                }
            />
        );
    }

    return <>{children}</>;
};

export default RolePermissionMiddleware;

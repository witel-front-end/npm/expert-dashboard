import Message from "@/core/components/Messages";
import useUser from "@/lib/app/hooks/useUser";
import {Stack, Typography} from "@mui/material";
import {useTranslations} from "next-intl";
import {useSearchParams} from "next/navigation";
import {useRouter} from "next/router";
import {useEffect} from "react";

const WithoutAuthMiddleware = ({children}) => {
    const {isAuth} = useUser();
    const t = useTranslations();
    const router = useRouter();

    // gettin url query
    const searchParams = useSearchParams();
    const backUrlDecodedPath = searchParams.get("back_url");

    useEffect(() => {
        if (!isAuth) return;
        const timer = setTimeout(() => {
            router.replace(
                backUrlDecodedPath
                    ? decodeURIComponent(backUrlDecodedPath)
                    : "/dashboard"
            );
        }, 2000);

        return () => {
            clearTimeout(timer);
        };
    }, [isAuth]);

    if (isAuth)
        return (
            <Message
                text={
                    <Stack alignItems="center" spacing={2}>
                        <Typography sx={{textAlign: "center"}}>
                            {t(
                                "Authorization.typography_your_login_is_valid_and_you_do_not_need_to_login_again"
                            )}
                        </Typography>
                        <Typography>
                            {t("Authorization.typography_redirect_to")}{" "}
                            {backUrlDecodedPath
                                ? t("Authorization.typography_routing_previuos_page")
                                : t("Authorization.typography_routing_dashbaord_page")}
                        </Typography>
                    </Stack>
                }
            />
        );
    return <>{children}</>;
};

export default WithoutAuthMiddleware;

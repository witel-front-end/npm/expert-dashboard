/** @type {import('next').NextConfig} */

const nextConfig = {
    reactStrictMode: true,
    i18n: {
        defaultLocale: 'fa',
        locales: ['fa'],
        localeDetection: false,
    }
}

module.exports = nextConfig
